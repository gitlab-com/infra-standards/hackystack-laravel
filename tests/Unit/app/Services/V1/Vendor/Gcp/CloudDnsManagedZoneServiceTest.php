<?php

namespace Tests\Unit\app\Services\V1\Vendor\Gcp;

use Tests\Fakes\CloudDnsManagedZoneServiceFake;

it('can check for existing dns zone', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsManagedZoneServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $response = $client->checkForDnsZone([
        'name' => 'testing-zone'
    ]);
    expect($response->status->code)->toBe(200);
});

it('will return 404 status if managed zone does not exists', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsManagedZoneServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $response = $client->checkForDnsZone([
        'name' => 'fake-zone-zone'
    ]);
    expect($response->status->code)->toBe(404);
});

it('can create a new managed zone', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsManagedZoneServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $response = $client->createDnsManagedZone([
        'name' => config('tests.gcp.gcp_dns_prefix') . 'testing-zone-4',
        'fqdn' => config('tests.gcp.gcp_dns_prefix') . 'testing-zone-4' . config('tests.gcp.gcp_dns_suffix'),
        'visibility' => 'public',
        'description' => 'Testing zone 3 by SDK',
        'logging_enabled' => true
    ]);
    expect($response->status->code)->toBe(200);
});

it('can delete a managed zone', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsManagedZoneServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $response = $client->deleteDnsManagedZone([
        'name' => config('tests.gcp.gcp_dns_prefix') . 'testing-zone-4'
    ]);
    expect($response->status->code)->toBe(200);
});
