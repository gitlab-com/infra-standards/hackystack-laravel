<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class CloudAccountEnvironmentService extends BaseService
{
    public function __construct()
    {
        $this->model = Models\Cloud\CloudAccountEnvironment::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      cloud_account_id        required|uuid|exists:cloud_accounts,id
     *      cloud_account_environment_template_id   required|uuid|exists:cloud_accounts_environments_templates,id
     *      cloud_organization_unit_id  required|uuid|exists:cloud_organization_units,id
     *      cloud_provider_id       required|uuid|exists:cloud_providers,id
     *      cloud_realm_id          required|uuid|exists:cloud_realms,id
     *      name                    required|string
     *      description             nullable|string
     *      expires_at              nullable|datetime
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        $record = new $this->model();

        // Get Auth Tenant relationship
        $auth_tenant = Models\Auth\AuthTenant::query()
            ->where('id', $request_data['auth_tenant_id'])
            ->firstOrFail();

        // Get Cloud Account relationship
        $cloud_account = Models\Cloud\CloudAccount::query()
            ->where('id', $request_data['cloud_account_id'])
            ->firstOrFail();

        // Get Cloud Account Environment Template relationship
        $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
            ->where('id', $request_data['cloud_account_environment_template_id'])
            ->firstOrFail();

        // Get Cloud Provider relationship
        $cloud_provider = Models\Cloud\CloudProvider::query()
            ->where('id', $request_data['cloud_provider_id'])
            ->firstOrFail();

        // Update value of record with ID of relationship
        $record->auth_tenant_id = $auth_tenant->id;
        $record->cloud_account_id = $cloud_account->id;
        $record->cloud_account_environment_template_id = $cloud_account_environment_template->id;
        $record->cloud_organization_unit_id = $cloud_account->cloud_organization_unit_id;
        $record->cloud_provider_id = $cloud_provider->id;
        $record->cloud_realm_id = $cloud_account->cloud_realm_id;

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->description = Arr::get($request_data, 'description');

        if (!empty($request_data['expires_at'])) {
            $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

            if ($expires_at < now()) {
                abort(400, 'The expires_at value cannot be in the past.');
            } else {
                $record->expires_at = $expires_at;
            }
        } else {
            $record->expires_at = null;
        }

        $record->save();

        // Update state after model is saved to override BaseModel behavior
        $record->state = Arr::get($request_data, 'state', 'pending');
        $record->save();

        // Use service method to provision Git projects
        // TODO Refactor into background jobs
        $this->provisionGitProject($record->id);
        $this->provisionTemplateGitCiVariables($record->id);
        $this->provisionGitOpsServiceAccount($record->id);
        $this->provisionDnsManagedZone($record->id);

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      name                    nullable|string|max:55
     *      description             nullable|string|max:255
     *      display_order           nullable|integer|between:1,99
     *      state                   nullable|string|in:active,inactive,archived
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        // TODO
    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    /**
     * Provision a Git project repository
     *
     * @param string $id
     *      CloudAccountEnvironment UUID
     *
     * @return bool
     *      (true) if provisioned, (false) if failed
     */
    public function provisionGitProject($id)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // If api_enabled is false, abort provisioning
        if (config('hackystack.auth.git.providers.gitlab.api_enabled') == false) {
            // TODO Log Git API not enabled
            return false;
        }

        // If AuthTenant Git credentials are empty, abort provisioning
        if ($record->authTenant->git_provider_base_url == null || $record->authTenant->git_provider_api_token == null) {
            // TODO Log Git API credentials not configured in AuthTenant model
            // abort(501, 'The Git API credentials have not been configured in the AuthTenant. No provisioning has occured.');
            return false;
        }

        // Define Git API credentials from AuthTenant
        $git_provider_type = $record->authTenant->git_provider_type;
        $git_provider_base_url = $record->authTenant->git_provider_base_url;
        $git_provider_api_token = decrypt($record->authTenant->git_provider_api_token);

        // Perform API call to create user in GitLab instance
        if ($git_provider_type == 'gitlab') {

            //Perform API call
            try {

                // Utilize HTTP to run a GET request
                $response = Http::withToken($git_provider_api_token)
                    ->post($git_provider_base_url . '/api/v4/projects', [
                        'name' => $record->name . '-env-' . $record->short_id,
                        'path' => $record->name . '-env-' . $record->short_id,
                        'namespace_id' => $record->cloudAccount->git_meta_data['id'],
                        'description' => 'This GitLab Project was programmatically created for a Cloud Account Environment that is managed by ' . config('hackystack.app.name') . ' (powered by HackyStack) to manage the Terraform configuration. Use merge requests to update the Terraform configuration and CI pipelines to execute Terraform commands.',
                        'auto_devops_enabled' => 'false',
                        'group_with_project_templates_id' => $record->cloudAccountEnvironmentTemplate->git_meta_data['namespace']['id'],
                        'template_project_id' => $record->cloudAccountEnvironmentTemplate->git_meta_data['id'],
                        'use_custom_template' => 'true',
                    ]);

                // Throw an exception if a client or server error occured.
                $response->throw();
            } catch (\Illuminate\Http\Client\RequestException $e) {
                dd($e);
                // TODO: Fix error handling to give us useful logs and outputs for debugging.
            }
        }

        $record->git_meta_data = (array) json_decode($response->body());
        $record->state = 'active';

        $record->save();

        return true;
    }

    public function provisionTemplateGitCiVariables($id)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount',
            'cloudAccountEnvironmentTemplate'
        ])->where('id', $id)
            ->firstOrFail();

        // Check if single cloud account user
        $cloud_account_users_count = $record->cloudAccount->cloudAccountUsers->count();

        // Get Auth User by ID.
        if ($cloud_account_users_count == 1) {
            foreach ($record->cloudAccount->cloudAccountUsers->take(1) as $cloud_account_user) {
                $auth_user = $cloud_account_user->authUser;
            }
        } elseif (Auth::check()) {
            $auth_user = Models\Auth\AuthUser::where('id', Auth::id())->first();
        } else {
            $auth_user = null;
        }

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            foreach ($record->cloudAccountEnvironmentTemplate->git_ci_variables as $ci_variable) {
                $ci_variable_value = $ci_variable['value'];
                $dynamic_type = $ci_variable['dynamic_type'];

                if ($dynamic_type == 'string') {
                    $value = $ci_variable['value'];
                } elseif ($dynamic_type == 'auth-group-namespace-slug') {
                    if ($auth_user != null) {
                        $auth_groups = $auth_user->authGroups()->where('namespace', $ci_variable_value)->get();

                        if (count($auth_groups) == 1) {
                            foreach ($auth_groups->take(1) as $auth_group) {
                                // Remove namespace prefix from slug
                                $value = str_replace($auth_group->namespace . '-', '', $auth_group->slug);
                            }
                        }
                    } else {
                        $value = null;
                    }
                } elseif ($dynamic_type == 'auth-user-string') {
                    if ($auth_user != null) {
                        $value = $auth_user->$ci_variable_value;
                    } else {
                        $value = null;
                    }
                } elseif ($dynamic_type == 'auth-user-provider-meta-data') {
                    if ($auth_user != null) {
                        $value = $auth_user->provider_meta_data[$ci_variable_value];
                    } else {
                        $value = null;
                    }
                } elseif ($dynamic_type == 'cloud-account-string') {
                    $value = $record->cloudAccount->$ci_variable_value;
                } elseif ($dynamic_type == 'cloud-account-api-meta-data') {
                    $value = $record->cloudAccount->api_meta_data[$ci_variable_value];
                } elseif ($dynamic_type == 'cloud-account-git-meta-data') {
                    $value = $record->cloudAccount->git_meta_data[$ci_variable_value];
                } elseif ($dynamic_type == 'cloud-account-dns-meta-data') {
                    $value = $record->cloudAccount->dns_meta_data[$ci_variable_value];
                } elseif ($dynamic_type == 'cloud-account-environment-string') {
                    $value = $record->$ci_variable_value;
                } elseif ($dynamic_type == 'cloud-account-environment-env-prefix-string') {
                    $value = 'env-' . $record->$ci_variable_value;
                } elseif ($dynamic_type == 'cloud-account-environment-git-meta-data') {
                    $value = $record->git_meta_data[$ci_variable_value];
                } elseif ($dynamic_type == 'cloud-organization-unit-string') {
                    $value = $record->cloudAccount->cloudOrganizationUnit->$ci_variable_value;
                } elseif ($dynamic_type == 'cloud-provider-string') {
                    $value = $record->cloudAccount->cloudProvider->$ci_variable_value;
                } elseif ($dynamic_type == 'cloud-realm-string') {
                    $value = $record->cloudAccount->cloudRealm->$ci_variable_value;
                } else {
                    $value = null;
                }

                $this->storeGitCiVariable($record->id, [
                    'id' => $record->git_meta_data['id'],
                    'key' => $ci_variable['key'],
                    'value' => $value,
                    'variable_type' => $ci_variable['variable_type'],
                    'protected' => $ci_variable['protected'],
                    'masked' => $ci_variable['masked'],
                    'environment_scope' => $ci_variable['environment_scope']
                ]);
            }

            $record->git_ci_variables = $this->listGitCiVariables($record->id);
            $record->save();
        }

        return true;
    }

    /**
     * Provision a Cloud Account Service Account IAM User for GitOps Terraform
     *
     * @param string $id
     *      Cloud Account Environment UUID
     *
     * @return bool
     *      True if successful
     *      False if not successful
     */
    public function provisionGitOpsServiceAccount($id)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        if ($record->cloudProvider->type == 'gcp') {

            // Create a service account
            $service_account_service = new \App\Services\V1\Vendor\Gcp\CloudIamServiceAccountService(
                $record->cloud_provider_id,
                $record->cloudAccount->api_meta_data['projectId']
            );

            $service_account = $service_account_service->store([
                'account_id' => 'gitops-ci-' . $record->short_id,
                'display_name' => 'GitOps CI ' . $record->short_id,
                'description' => 'This is a HackyStack generated service account for Terraform GitOps Environments.'
            ]);

            // TODO Determine sleep requirement for API delay
            sleep(2);

            // Add roles to service account
            $service_account_service = new \App\Services\V1\Vendor\Gcp\CloudResourceManagerIamUserRoleService(
                $record->cloud_provider_id,
                $record->cloudAccount->api_meta_data['projectId']
            );

            $service_account_service->attach(
                'roles/owner',
                'serviceAccount:' . $service_account->object->email
            );

            // Create a service account key
            $service_account_key_service = new \App\Services\V1\Vendor\Gcp\CloudIamServiceAccountKeyService(
                $record->cloud_provider_id,
                $record->cloudAccount->api_meta_data['projectId']
            );

            $service_account_key = $service_account_key_service->store($service_account->object->uniqueId);

            // TODO Check if service account and key was created successfully

            // Update GitLab Project CI/CD Variable
            $this->storeGitCiVariable($id, [
                'id' => $record->git_meta_data['id'],
                'key' => 'GOOGLE_APPLICATION_CREDENTIALS',
                'value' => base64_decode($service_account_key->object->privateKeyData),
                'variable_type' => 'file',
                'protected' => false,
                'masked' => false,
                'environment_scope' => '*'
            ]);

            return true;
        } elseif ($record->cloudProvider->type == 'aws') {
            // TODO
        }

        return false;
    }

    /**
     * Rotate the token for a Cloud Account Service Account IAM User for GitOps Terraform
     *
     * @param string $id
     *      Cloud Account Environment UUID
     *
     * @return bool
     *      True if successful
     *      False if not successful
     */
    public function rotateGitOpsServiceAccountKey($id)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        if ($record->cloudProvider->type == 'gcp') {

            // Create a service account key
            $service_account_key_service = new \App\Services\V1\Vendor\Gcp\CloudIamServiceAccountKeyService(
                $record->cloud_provider_id,
                $record->cloudAccount->api_meta_data['projectId']
            );

            // Define service account email string
            $iam_email = 'gitops-ci-' . $record->short_id . '@' . $record->cloudAccount->api_meta_data['projectId'] . '.iam.gserviceaccount.com';

            // Get list of service account keys
            $service_account_keys = $service_account_key_service->list($iam_email);

            // Parse key ID from service account keys object details
            $key_id = collect($service_account_keys->object->keys)
                ->where('keyType', 'USER_MANAGED')
                ->transform(function ($item, $key) {
                    return explode('keys/', $item->name)[1];
                })
                ->first();

            // Delete the existing key for the service account
            $service_account_key_service->delete($iam_email, $key_id);

            // Create a new key for the service account
            $new_service_account_key = $service_account_key_service->store($iam_email);

            // Update GitLab Project CI/CD Variable
            $this->updateGitCiVariable($id, [
                'id' => $record->git_meta_data['id'],
                'key' => 'GOOGLE_APPLICATION_CREDENTIALS',
                'value' => base64_decode($new_service_account_key->object->privateKeyData),
                'variable_type' => 'file',
                'protected' => false,
                'masked' => false,
                'environment_scope' => '*'
            ]);

            return true;
        } elseif ($record->cloudProvider->type == 'aws') {
            // TODO
        }

        return false;
    }

    /**
     * Provision a Cloud Account Service Account IAM User for GitOps Terraform
     *
     * @param string $id
     *      Cloud Account Environment UUID
     *
     * @return bool
     *      True if successful
     *      False if not successful
     */
    public function provisionDnsManagedZone($id)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        if ($record->cloudProvider->type == 'gcp') {

            // Initialize service for the base domain. This is the organization
            // level domain that subdomain NS records are created in.
            $record_set_dns_service = new \App\Services\V1\Vendor\Gcp\CloudDnsRecordSetService(
                $record->cloud_provider_id,
                config('hackystack.cloud.accounts.gcp.environments.dns.base.gcp_project')
            );

            // Initialize service for Cloud Account Environment managed zone
            $managed_zone_dns_service = new \App\Services\V1\Vendor\Gcp\CloudDnsManagedZoneService(
                $record->cloud_provider_id,
                $record->cloudAccount->api_meta_data['projectId']
            );

            // Create a managed zone in the Cloud Account GCP project
            $env_managed_zone = $managed_zone_dns_service->createDnsManagedZone([
                'name' => config('hackystack.cloud.accounts.gcp.environments.dns.env.prefix') . $record->short_id,
                'fqdn' => config('hackystack.cloud.accounts.gcp.environments.dns.env.prefix') . $record->short_id . '.' . config('hackystack.cloud.accounts.gcp.environments.dns.base.zone_fqdn'),
                'visibility' => 'public',
                'logging_enabled' => true,
                'description' => 'This is a HackyStack generated DNS Managed Zone.'
            ]);

            // Create a DNS NS record in the organization managed zone for the new subdomain zone.
            $record_set_dns_service->createDnsRecordSet([
                'managed_zone' => config('hackystack.cloud.accounts.gcp.environments.dns.base.zone_name'),
                'project_id' => config('hackystack.cloud.accounts.gcp.environments.dns.base.gcp_project'),
                'name' => config('hackystack.cloud.accounts.gcp.environments.dns.env.prefix') . $record->short_id . '.' . config('hackystack.cloud.accounts.gcp.environments.dns.base.zone_fqdn'),
                'type' => 'NS',
                'rrdatas' => $env_managed_zone->object->nameServers
            ]);

            return true;
        } elseif ($record->cloudProvider->type == 'aws') {
            // TODO
        }

        return false;
    }

    public function listGitCiVariables($id)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            $response = Http::withToken(decrypt($record->authTenant->git_provider_api_token))
                ->get($record->authTenant->git_provider_base_url . '/api/v4/projects/' . $record->git_meta_data['id'] . '/variables');

            // Throw an exception if a client or server error occurred.
            $response->throw();

            return $response->object();
        }
    }

    public function checkIfGitCiVariableExists($id, $key)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            $response = Http::withToken(decrypt($record->authTenant->git_provider_api_token))
                ->get($record->authTenant->git_provider_base_url . '/api/v4/projects/' . $record->git_meta_data['id'] . '/variables');

            // Throw an exception if a client or server error occurred.
            $response->throw();

            // Use Laravel Collection to search the `key` value and return boolean if found
            // https://laravel.com/docs/8.x/collections#method-pluck
            // https://laravel.com/docs/8.x/collections#method-contains
            $collection = collect($response->object());
            $collection_check = $collection->pluck('key')->contains($key);

            return $collection_check;
        }
    }

    public function getGitCiVariable($id, $key)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            $response = Http::withToken(decrypt($record->authTenant->git_provider_api_token))
                ->get($record->authTenant->git_provider_base_url . '/api/v4/projects/' . $record->git_meta_data['id'] . '/variables/' . $key);

            // Throw an exception if a client or server error occurred.
            $response->throw();

            return $response->object();
        }
    }

    /**
     *   Store a CI Variable in the Git Project
     *
     *   @see https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable
     *
     *   @param uuid  $id               CloudAccountEnvironment UUID
     *   @param array $request_data
     *       id|int                     GitLab project ID
     *       key|string                 CI Variable Key (Ex. MY_ENVIRONMENT_VARIABLE)
     *       value|string               CI Variable Value
     *       variable_type|enum         Type of GitLab CI variable (env_var|file)
     *       protected|bool             If set, this variable will only apply to protected branches (ex. master).
     *       masked|bool                If set, this variable will be masked in console outputs.
     *       environment_scope|string   If set, this variable will only apply to specific environments. Default *
     *
     *   @return object   GitLab API Response with CI Variable
     */
    public function storeGitCiVariable($id, $request_data = [])
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            $response = Http::withToken(decrypt($record->authTenant->git_provider_api_token))
                ->post($record->authTenant->git_provider_base_url . '/api/v4/projects/' . $record->git_meta_data['id'] . '/variables', [
                    'id' => $record->git_meta_data['id'],
                    'key' => $request_data['key'],
                    'value' => $request_data['value'],
                    'variable_type' => array_key_exists('variable_type', $request_data) ? $request_data['variable_type'] : 'env_var',
                    'protected' => array_key_exists('protected', $request_data) ? $request_data['protected'] : 'false',
                    'masked' => array_key_exists('masked', $request_data) ? $request_data['masked'] : 'false',
                    'environment_scope' => array_key_exists('environment_scope', $request_data) ? $request_data['environment_scope'] : '*',
                ]);

            // Throw an exception if a client or server error occurred.
            $response->throw();

            return $response->object();
        }
    }

    public function updateGitCiVariable($id, $request_data = [])
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            $existing_variable = $this->getGitCiVariable($id, $request_data['key']);

            $response = Http::withToken(decrypt($record->authTenant->git_provider_api_token))
                ->put($record->authTenant->git_provider_base_url . '/api/v4/projects/' . $record->git_meta_data['id'] . '/variables/' . $request_data['key'], [
                    'id' => $record->git_meta_data['id'],
                    'key' => $request_data['key'],
                    'value' => (array_key_exists('value', $request_data) ? $request_data['value'] : $existing_variable->value),
                    'variable_type' => (array_key_exists('variable_type', $request_data) ? $request_data['variable_type'] : $existing_variable->variable_type),
                    'protected' => (array_key_exists('protected', $request_data) ? $request_data['protected'] : $existing_variable->protected),
                    'masked' => (array_key_exists('masked', $request_data) ? $request_data['masked'] : $existing_variable->masked),
                    'environment_scope' => (array_key_exists('environment_scope', $request_data) ? $request_data['environment_scope'] : $existing_variable->environment_scope),
                ]);

            // Throw an exception if a client or server error occurred.
            $response->throw();

            return $response->object();
        }
    }

    public function deleteGitCiVariable($id, $key)
    {
        // Get record by ID
        $record = $this->model()->with([
            'authTenant',
            'cloudAccount'
        ])->where('id', $id)
            ->firstOrFail();

        // Perform API call to create user in GitLab instance
        if ($record->authTenant->git_provider_type == 'gitlab') {
            $existing_variable = $this->getGitCiVariable($id, $key);

            $response = Http::withToken(decrypt($record->authTenant->git_provider_api_token))
                ->delete($record->authTenant->git_provider_base_url . '/api/v4/projects/' . $record->git_meta_data['id'] . '/variables/' . $key);

            // Throw an exception if a client or server error occurred.
            $response->throw();

            return $response->successful();
        }
    }
}
