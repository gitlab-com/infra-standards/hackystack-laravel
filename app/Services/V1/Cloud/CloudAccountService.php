<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CloudAccountService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudAccount::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id                  required|uuid|exists:auth_tenants,id
     *      cloud_billing_account_id        required|uuid|exists:cloud_billing_accounts,id
     *      cloud_organization_unit_id      required|uuid|exists:cloud_organization_units,id
     *      cloud_provider_id               required|uuid|exists:cloud_providers,id
     *      cloud_realm_id                  required|uuid|exists:cloud_realms,id
     *      api_meta_data                   nullable|array
     *      name                            required|string|max:255
     *      slug                            required|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get Cloud Provider relationship
        if(!empty($request_data['cloud_provider_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('id', $request_data['cloud_provider_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_provider_id = $cloud_provider->id;
        }

        // Get Cloud Realm relationship
        if(!empty($request_data['cloud_realm_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('id', $request_data['cloud_realm_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_realm_id = $cloud_realm->id;
        }

        // Get Cloud Billing Account relationship
        if(!empty($request_data['cloud_billing_account_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('id', $request_data['cloud_billing_account_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_billing_account_id = $cloud_billing_account->id;

        } else {

            // Use default value from cloud realm for billing account
            $record->cloud_billing_account_id = $cloud_realm->cloud_billing_account_id;

        }

        // Get Cloud Organization Unit relationship
        if(!empty($request_data['cloud_organization_unit_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('id', $request_data['cloud_organization_unit_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_organization_unit_id = $cloud_organization_unit->id;
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');

        // If api meta data is in request, encode as JSON
        if(Arr::has($request_data, 'api_meta_data') && count($request_data['api_meta_data']) > 0) {
            $record->api_meta_data = json_encode($request_data['api_meta_data']);
        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Provision the Cloud Account
        $this->provision($record->id);

        // Initialize role service for creating default roles
        $cloudAccountRoleService = new CloudAccountRoleService();

        // Loop through the default roles for the cloud provider and create a
        // new database record for each role that can be associated with a user
        foreach($record->cloudProvider->default_roles as $role_name) {
            $cloudAccountRoleService->store([
                'auth_tenant_id' => $record->auth_tenant_id,
                'cloud_account_id' => $record->id,
                'cloud_provider_id' => $record->cloud_provider_id,
                'cloud_realm_id' => $record->cloud_realm_id,
                'api_name' => $role_name
            ]);
        }

        // Provision the Git Group
        $this->provisionGitGroup($record->id);

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      cloud_billing_account_id        nullable|uuid|exists:cloud_billing_accounts,id
     *      cloud_organization_unit_id      nullable|uuid|exists:cloud_organization_units,id
     *      cloud_realm_id                  nullable|uuid|exists:cloud_realms,id
     *      api_meta_data                   nullable|array
     *      name                            required|string|max:255
     *      slug                            required|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Get Cloud Realm relationship
        if(!empty($request_data['cloud_realm_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_realm_id != $request_data['cloud_realm_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_realm = Models\Cloud\CloudRealm::query()
                    ->where('id', $request_data['cloud_realm_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_realm_id = $cloud_realm->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different realm. In this iteration, this
                // only affects the database meta data and doesn't perform any
                // infrastructure changes.

            }
        }

        // Get Cloud Billing Account relationship
        if(!empty($request_data['cloud_billing_account_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_billing_account_id != $request_data['cloud_billing_account_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                    ->where('id', $request_data['cloud_billing_account_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_billing_account_id = $cloud_billing_account->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different billing account. In this
                // iteration, this only affects the database meta data and doesn't
                // perform any infrastructure changes.

            }
        }

        // Get Cloud Organization Unit relationship
        if(!empty($request_data['cloud_organization_unit_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_organization_unit_id != $request_data['cloud_organization_unit_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                    ->where('id', $request_data['cloud_organization_unit_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_organization_unit_id = $cloud_organization_unit->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different organization unit. In this
                // iteration, this only affects the database meta data and doesn't
                // perform any infrastructure changes.

            }
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);

        // If api meta data is in request, encode as JSON
        // TODO Use collect() methods to perform key matching and only update changed values to avoid data loss
        if(Arr::has($request_data, 'api_meta_data') && count($request_data['api_meta_data']) > 0) {
            $record->api_meta_data = json_encode($request_data['api_meta_data']);
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    /**
     *   Provision the Cloud Account using the API of the Cloud Provider
     *
     *   After the database record has been created, this method handles the
     *   business logic of determining which cloud provider to use and calls
     *   the respective App\Services\V1\Vendor\* class methods to provision
     *   the infrastructure using the cloud provider API/SDK.
     *
     *   The response is returned to this method and the database is updated
     *   with the api_meta_data and the flag_provisioned boolean.
     *
     *   The provisioned_at timestamp is updated in the model setter
     *   method App\Cloud\CloudAccount::setFlagProvisionedAttribute().
     *
     *   @param  uuid $id   Cloud Account UUID
     *
     *   @return bool       Provisioning successful (true|false)
     */
    public function provision($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Check that flag_provisioned is null and organization unit exists
        if($record->flag_provisioned == false && $record->cloudOrganizationUnit->flag_provisioned == true) {

            // GCP Project Creation
            if($record->cloudProvider->type == 'gcp') {

                // Initialize GCP Project Service
                $projectService = new Services\V1\Vendor\Gcp\CloudResourceManagerProjectService($record->cloud_provider_id);

                // Use service method to provision with API
                // Note: The project name supports a maximum of 30 characters.
                // With the concatenation of the 8-char short_id and hyphen, we
                // only allow a maxiumum of 21 characters so we perform string
                // length shortening on longer slugs.
                $api_meta_data = $projectService->provision([
                    'project_name' => Str::limit($record->slug, 21, '').'-'.$record->short_id,
                    'parent_folder_name' => $record->cloudOrganizationUnit->api_meta_data['name'],
                    //'labels' => $this->generateTags($record->id)
                ]);

                // Update the database record
                $record->api_meta_data = $api_meta_data;

                // Get response state. There is no expected state value for
                // failure with provisioning.
                // https://cloud.google.com/resource-manager/reference/rest/v3/projects#State
                if($api_meta_data['state'] == 'ACTIVE') {
                    $record->flag_provisioned = 1;
                    $record->state = 'active';
                    $record->save();
                } else {
                    $record->flag_provisioned = 0;
                    $record->state = 'provisioning-error';
                    $record->save();
                }

                // Allow time for project services to become available before running additional commands
                // TODO Refactor into queued jobs
                sleep(5);

                if($record->cloudProvider->type == 'gcp') {

                    // Use method in this class to update billing account
                    $this->provisionBillingAccount($record->id);

                }

                return true;

            }

            // AWS Account Creation
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Organizations Account Service
                $accountService = new Services\V1\Vendor\Aws\OrganizationAccountService($record->cloud_provider_id);

                // Root Email Configuration
                // When creating a globally unique root email address for AWS,
                // a HackyStack administrator needs access to the email account
                // to be able to perform password resets and delete the account.
                //
                // Plus Method - Creates an alias on an email account or group
                // that HackyStack administrators can access.
                // {plus_prefix}+{cloud_account_short_id}@{plus_domain}
                // Ex. `gitlab-aws-master-accounts+a1b2c3d4@gitlab.com`

                if(config('hackystack.cloud.accounts.aws.root_email.method') == 'plus') {

                    if(config('hackystack.cloud.accounts.aws.root_email.plus_prefix') == null) {

                        $record->api_meta_data['FAILURE_REASON'] = 'HackyStack AWS root email prefix is null.';
                        $record->state = 'provisioning-error';
                        $record->save();

                    }

                    elseif(config('hackystack.cloud.accounts.aws.root_email.plus_domain') == null) {

                        $record->api_meta_data['FAILURE_REASON'] = 'HackyStack AWS root email domain is null.';
                        $record->state = 'provisioning-error';
                        $record->save();

                    }

                    else {

                        // Use service method to provision with API
                        $api_meta_data = $accountService->create([
                            'name' => $record->slug.'-'.$record->short_id,
                            'email' => config('hackystack.cloud.accounts.aws.root_email.plus_prefix').'+'.$record->short_id.'@'.config('hackystack.cloud.accounts.aws.root_email.plus_domain'),
                            'billing_access' => true,
                            'tags' => $this->generateTags($record->id),
                        ]);

                        // Update the database record
                        $record->api_meta_data = $api_meta_data;
                        $record->flag_provisioned = 1;

                        if($api_meta_data['State'] == 'IN_PROGRESS') {
                            $record->state = 'provisioning';
                            $record->save();
                        } elseif($api_meta_data['State'] == 'SUCCEEDED') {
                            $record->state = 'active';
                            $record->save();
                        } elseif($api_meta_data['State'] == 'FAILED') {
                            $record->state = 'provisioning-error';
                            $record->save();
                        }

                        return true;

                    }

                } else {

                    $record->api_meta_data['FAILURE_REASON'] = 'HackyStack AWS root email method is invalid.';
                    $record->state = 'provisioning-error';
                    $record->save();

                    return false;

                }

            }

        } // if($record->flag_provisioned == false)

        else {
            $record->state = 'provisioning-pending';
            $record->save();
        }

        return false;

    }

    public function deprovision($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Check that is provisioned
        if($record->flag_provisioned == 1) {

            // GCP Folder Deletion
            if($record->cloudProvider->type == 'gcp') {

                // Initialize GCP Project Service
                $projectService = new Services\V1\Vendor\Gcp\CloudResourceManagerFolderService($record->cloud_provider_id);

                // Use service method to provision with API
                $api_meta_data = $projectService->deprovision([
                    'project_name' => Str::limit($record->slug, 21).'-'.$record->short_id,
                ]);

                // Update the database record
                $record->api_meta_data = $api_meta_data;
                $record->flag_provisioned = 0;

                // FIXME: Run the request in Tinker to get expected output and
                // refactor to the lifecycleState output (object or array). The
                // $api_meta_data['state'] == 'ACTIVE' is from AWS, not GCP.
                //
                // FIXME: The state should be deprovisioned or similar. Need to
                // check what model enum field allows.
                //
                if($api_meta_data['state'] == 'ACTIVE') {
                    $record->state = 'provisioning';
                    $record->save();
                } else {
                    $record->state = 'provisioning-error';
                    $record->save();
                }

                return true;

            }

            // AWS Organization Unit Deletion
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Organizations Organization Unit Service
                $accountService = new Services\V1\Vendor\Aws\OrganizationAccountService($record->cloud_provider_id);

                // Use service method to deprovision with API
                $api_meta_data = $accountService->delete($record->api_meta_data['Id']);

            }

            // FIXME: Do not delete api_meta_data.
            //
            // FIXME: This needs to move into AWS method.
            //
            // Update the database record
            $record->api_meta_data = null;
            $record->flag_provisioned = 0;
            $record->save();

        } // if($record->flag_provisioned == 1)
    }

    public function generateTags($id, $auth_user_id = null)
    {

        // Tags
        // ------------------------------------------------------------
        // You can set a dynamic list of tags that will be added to each
        // cloud account. You can specify a different set of tags for
        // each realm.
        //
        // Supported Types:
        // 'string'
        // 'auth-group-namespace-slug'
        // 'auth-user-string'
        // 'auth-user-provider-meta-data'
        // 'cloud-account-string'
        // 'cloud-organization-unit-string'
        // 'cloud-realm-string'
        // 'cloud-provider-string'
        //

        // Get Cloud Account by ID
        $cloud_account = $this->model()->with([
                'cloudRealm',
                'cloudOrganizationUnit',
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Get Auth User by ID.
        if($auth_user_id != null) {
            $auth_user = Models\Auth\AuthUser::where('id', $auth_user_id)->first();
        } else {
            $auth_user = Models\Auth\AuthUser::where('id', Auth::id())->first();
        }

        // If cloud provider type is AWS
        if($cloud_account->cloudProvider->type == 'aws') {

            // Get array of tags from the configuration file
            $tag_schema = config('hackystack.cloud.accounts.aws.tags');

            // Check if cloud realm slug is in the array
            if(array_key_exists($cloud_account->cloudRealm->slug, $tag_schema)) {

                // Define variable with nested array for the cloud realm
                $realm_tag_schema = $tag_schema[$cloud_account->cloudRealm->slug];

                // Define an empty array to ammend with realm tags
                $cloud_account_tags = [];

                // Loop through the tags for this realm
                foreach($realm_tag_schema as $realm_tag) {

                    // Cast array value to string (strongly typed)
                    $value = (string) $realm_tag['value'];

                    // Static string
                    if($realm_tag['type'] == 'string') {
                        $cloud_account_tags[] = [
                            'Key' => $realm_tag['key'],
                            'Value' => Str::slug($value),
                        ];
                    }

                    // Slug of authentication group model if single result is found
                    // for user in a specific namespace
                    elseif($realm_tag['type'] == 'auth-group-namespace-slug') {
                        if($auth_user) {

                            $auth_groups = $auth_user->AuthGroups()->where('namespace', $value)->get();

                            if(count($auth_groups) == 1) {
                                foreach($auth_groups->take(1) as $auth_group) {

                                    // Remove namespace prefix from slug
                                    $slug = str_replace($auth_group->namespace.'-', '', $auth_group->slug);

                                    $cloud_account_tags[] = [
                                        'Key' => $realm_tag['key'],
                                        'Value' => Str::slug($slug),
                                    ];

                                }
                            }
                        }
                    }

                    // String using user model.
                    elseif($realm_tag['type'] == 'auth-user-string') {
                        if($auth_user) {
                            $cloud_account_tags[] = [
                                'Key' => $realm_tag['key'],
                                'Value' => Str::slug($auth_user->$value),
                            ];
                        }
                    }

                    // String using user authentication provider meta data.
                    elseif($realm_tag['type'] == 'auth-user-provider-meta-data') {
                        if($auth_user) {
                            $cloud_account_tags[] = [
                                'Key' => $realm_tag['key'],
                                'Value' => Str::slug($auth_user->provider_meta_data[$value]),
                            ];
                        }
                    }

                    // String using cloud account model
                    elseif($realm_tag['type'] == 'cloud-account-string') {
                        $cloud_account_tags[] = [
                            'Key' => $realm_tag['key'],
                            'Value' => Str::slug($cloud_account->$value),
                        ];
                    }

                    // String using cloud account model parent cloud organization unit
                    elseif($realm_tag['type'] == 'cloud-organization-unit-string') {
                        $cloud_account_tags[] = [
                            'Key' => $realm_tag['key'],
                            'Value' => Str::slug($cloud_account->cloudOrganizationUnit->$value),
                        ];
                    }

                    // String using cloud account model parent cloud realm
                    elseif($realm_tag['type'] == 'cloud-realm-string') {
                        $cloud_account_tags[] = [
                            'Key' => $realm_tag['key'],
                            'Value' => Str::slug($cloud_account->cloudRealm->$value),
                        ];
                    }

                    // String using cloud account model parent cloud provider
                    elseif($realm_tag['type'] == 'cloud-provider-string') {
                        $cloud_account_tags[] = [
                            'Key' => $realm_tag['key'],
                            'Value' => Str::slug($cloud_account->cloudProvider->$value),
                        ];
                    }

                    else {
                        // Ignore invalid tag type
                    }

                }

                return $cloud_account_tags;

            } else {
                // If no tags are specified for this realm, return an empty array
                return [];
            }

        }

        // If cloud provider type is GCP
        if($cloud_account->cloudProvider->type == 'gcp') {

            // Get array of labels from the configuration file
            $label_schema = config('hackystack.cloud.accounts.gcp.labels');

            // Check if cloud realm slug is in the array
            if(array_key_exists($cloud_account->cloudRealm->slug, $label_schema)) {

                // Define variable with nested array for the cloud realm
                $realm_label_schema = $label_schema[$cloud_account->cloudRealm->slug];

                // Define an empty array to ammend with realm labels
                $cloud_account_labels = [];

                // Loop through the labels for this realm
                foreach($realm_label_schema as $realm_label) {

                    // Cast array value to string (strongly typed)
                    $value = (string) $realm_label['value'];

                    // Static string
                    if($realm_label['type'] == 'string') {
                        $cloud_account_labels[] = [
                            $realm_label['key'] => Str::slug($value),
                        ];
                    }

                    // String using user model.
                    elseif($realm_label['type'] == 'auth-user-string') {
                        if($auth_user) {
                            $cloud_account_labels[] = [
                                $realm_label['key'] => Str::slug($auth_user->$value),
                            ];
                        }
                    }

                    // String using user authentication provider meta data.
                    elseif($realm_label['type'] == 'auth-user-provider-meta-data') {
                        if($auth_user) {
                            $cloud_account_labels[] = [
                                $realm_label['key'] => Str::slug($auth_user->provider_meta_data[$value]),
                            ];
                        }
                    }

                    // String using cloud account model
                    elseif($realm_label['type'] == 'cloud-account-string') {
                        $cloud_account_labels[] = [
                            $realm_label['key'] => Str::slug($cloud_account->$value),
                        ];
                    }

                    // String using cloud account model parent cloud organization unit
                    elseif($realm_label['type'] == 'cloud-organization-unit-string') {
                        $cloud_account_labels[] = [
                            $realm_label['key'] => Str::slug($cloud_account->cloudOrganizationUnit->$value),
                        ];
                    }

                    // String using cloud account model parent cloud realm
                    elseif($realm_label['type'] == 'cloud-realm-string') {
                        $cloud_account_labels[] = [
                            $realm_label['key'] => Str::slug($cloud_account->cloudRealm->$value),
                        ];
                    }

                    // String using cloud account model parent cloud provider
                    elseif($realm_label['type'] == 'cloud-provider-string') {
                        $cloud_account_labels[] = [
                            $realm_label['key'] => Str::slug($cloud_account->cloudProvider->$value),
                        ];
                    }

                    else {
                        // Ignore invalid label type
                    }

                }

                return $cloud_account_labels;

            } else {
                // If no tags are specified for this realm, return an empty array
                return [];
            }

        }

    }

    /**
     *   Provision Billing Account for GCP Cloud Account
     *
     *   @param  uuid $id       Cloud Account UUID
     *
     *   @return array      Billing Account API Response
     *      billingAccountName: "billingAccounts/017B02-778F9C-493B83",
     *      billingEnabled: true,
     *      name: "projects/achueshev-4ed36838/billingInfo",
     *      projectId: "achueshev-4ed36838",
     */
    public function provisionBillingAccount($id) {

        // Get Cloud Account by ID
        $cloud_account = $this->model()->with([
                'cloudBillingAccount',
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        if($cloud_account->cloudProvider->type == 'gcp') {

            // Initialize service for GCP Billing Account API
            $billingService = new Services\V1\Vendor\Gcp\CloudBillingProjectService($cloud_account->cloud_provider_id);

            // Use service method to attach billing account in GCP
            if($cloud_account->cloudBillingAccount->api_meta_data != null && array_key_exists('name', $cloud_account->cloudBillingAccount->api_meta_data)) {
                $billing_account = $billingService->update([
                    'project_name' => $cloud_account->api_meta_data['name'],
                    'billing_account_id' => $cloud_account->cloudBillingAccount->api_meta_data['name']
                ]);
            }

            return $billing_account;

        } else {

            abort(400, 'The provisionBillingAccount method can only be used with a Cloud Account that uses a `gcp` Cloud Provider. This Cloud Account ('.$cloud_account->short_id.') uses a `'.$cloud_account->cloudProvider->type.'` Cloud Provider.');

        }

    }

    /**
     *   Create a subgroup on the Git provider
     *
     *   @param  uuid $id       Cloud Account ID
     *
     *   @return bool
     */
    public function provisionGitGroup($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudOrganizationUnit'
            ])->where('id', $id)
            ->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->authTenant->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->authTenant->id);

            // If Git meta data is null, perform a create action
            if($record->git_meta_data == null) {

                // Check if organization unit has Git meta data (parent Git group exists)
                if($record->cloudOrganizationUnit->git_meta_data == null) {

                    $cloudOrganizationUnitService = new \App\Services\V1\Cloud\CloudOrganizationUnitService();
                    $cloudOrganizationUnitService->provisionGitGroup($record->cloudOrganizationUnit->id);

                    $cloud_organization_unit = \App\Models\Cloud\CloudOrganizationUnit::query()
                        ->where('id', $record->cloud_organization_unit_id)
                        ->firstOrFail();

                } else {

                    $cloud_organization_unit = $record->cloudOrganizationUnit;

                }

                // Perform API call to create group
                $cloud_account_group = $gitlabGroupService->create([
                    'name' => $record->slug.'-account-'.$record->short_id,
                    'path' => $record->slug.'-'.$record->short_id,
                    'description' => 'This GitLab Group was programmatically created for a Cloud Account that is managed by '.config('hackystack.app.name').' (powered by HackyStack). Do not make changes to the configuration for this group. ',
                    'parent_id' => $cloud_organization_unit->git_meta_data['id']
                ]);

            }

            // If Git meta data already exists, assume already provisioned and
            // perform an update instead of a create action
            elseif($record->git_meta_data != null) {

                // Perform API call to update group
                $cloud_account_group = $gitlabGroupService->update($record->git_meta_data['id'], [
                    'name' => $record->slug.'-account-'.$record->short_id,
                    'path' => $record->slug.'-'.$record->short_id,
                    'description' => 'This GitLab Group was programmatically created for a Cloud Account that is managed by '.config('hackystack.app.name').' (powered by HackyStack). Do not make changes to the configuration for this group. ',
                ]);

            }

            // Update the database with the API result
            $record->git_meta_data = $cloud_account_group;
            $record->save();

            return true;

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

    /**
     *   Destroy the subgroup on the Git provider
     *
     *   @param  uuid $id       Cloud Realm ID
     *
     *   @return bool
     */
    public function destroyGitGroup($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->withTrashed()->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->authTenant->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->authTenant->id);

            // If Git meta data is not null, perform deletion
            if($record->git_meta_data != null) {

                // Perform API call to destroy group
                $gitlab_group = $gitlabGroupService->destroy($record->git_meta_data['id']);

                // Update the database with the API result
                $record->git_meta_data = null;
                $record->save();

                return true;

            }

            // If Git meta data is not set, assume group does not exist
            elseif($record->git_meta_data == null) {
                return false;
            }

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

}
