<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class HackystackBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hackystack:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform a backup of the HackyStack .env file, config files, and database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line('');
        $this->line('<fg=cyan>---------------------------------------------------------------</>');
        $this->line('<fg=cyan> Backing up HackyStack configuration                           </>');
        $this->line('<fg=cyan>---------------------------------------------------------------</>');
        $this->line('');

        // Validate that backup directory exists or create it
        if(!is_dir(storage_path('backup'))) {
            File::makeDirectory(storage_path('backup'));
            $this->comment('Created: Backup directory created at '.storage_path('backup'));
        }

        // Validate that backup env directory exists or create it
        if(!is_dir(storage_path('backup/env'))) {
            File::makeDirectory(storage_path('backup/env'));
            $this->comment('Created: Backup directory for .env files created at '.storage_path('backup/env'));
        }

        // Validate that backup config directory exists or create it
        if(!is_dir(storage_path('backup/config'))) {
            File::makeDirectory(storage_path('backup/config'));
            $this->comment('Created: Backup directory for config files created at '.storage_path('backup/config'));
        }

        // Validate that .env file exists
        if(is_file(base_path('.env'))) {
            $this->comment('Created: Backing up `.env` to `storage/backup/env/.env.backup-'.\Carbon\Carbon::now()->format('Y-m-d-U'));
            File::copy(base_path('.env'), storage_path('backup/env/.env.backup-'.\Carbon\Carbon::now()->format('Y-m-d-U')));
        } else {
            $this->line('<fg=red>Skipped: No environment configuration exists at `.env`</>');
        }

        // Validate that .env file exists
        if(is_file(config_path('hackystack.php'))) {
            $this->comment('Created: Backing up `config/hackystack.php` to `storage/backup/config/hackystack.php.backup-'.\Carbon\Carbon::now()->format('Y-m-d-U'));
            File::copy(config_path('hackystack.php'), storage_path('backup/config/hackystack.php.backup-'.\Carbon\Carbon::now()->format('Y-m-d-U')));
        } else {
            $this->line('<fg=red>Skipped: No configuration exists at `config/hackystack.php`</>');
        }

        // Confirmation message
        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=green;options=bold>HackyStack backup complete. Please check for any errors above.</>');
        $this->line('<fg=red>Warning: Your database has not been backed up. Please use your preferred `mysqldump` tool to export your database.</>');
        $this->line('');

    }
}
