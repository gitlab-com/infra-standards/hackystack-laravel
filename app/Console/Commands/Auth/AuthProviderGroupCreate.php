<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class AuthProviderGroupCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-group:create
                            {--T|auth_tenant_slug= : The slug of the tenant this provider group belongs to}
                            {--P|auth_provider_slug= : The slug of the Authentication Provider}
                            {--G|auth_group_slug= : The slug of the Authentication Group to associate users with}
                            {--type= : The type of group association for provider users (default|meta)}
                            {--meta_key= : The key in the provider meta data to match. Not required for default type.}
                            {--meta_value= : The value for the key in the provider meta data to match. No required for default type.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Authentication Provider Group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Provider Groups - Create Record');

        //
        // Authentication Tenant
        // --------------------------------------------------------------------
        // Lookup the authentication tenant or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the group exist in?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Authentication Provider
        // --------------------------------------------------------------------
        // Lookup the authentication provider or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth provider0 from console option
        $auth_provider_slug = $this->option('auth_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($auth_provider_slug) {
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $auth_providers = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Provider Slug'],
                $auth_providers
            );

            $auth_provider_prompt = $this->anticipate('Which provider will users be authenticating with?', Arr::flatten($auth_providers));

            // Lookup provider based on slug provided in prompt
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_provider_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_provider) {
            $this->error('Error: No provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Authentication Group
        // --------------------------------------------------------------------
        // Lookup the authentication group or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth group from console option
        $auth_group_slug = $this->option('auth_group_slug');

        // If group slug option was specified, lookup by slug
        if($auth_group_slug) {
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_group_slug)
                ->first();
        }

        // If group slug was not provided, prompt for input
        else {

            // Get list of groups to show in console
            $auth_groups = Models\Auth\AuthGroup::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Group Slug'],
                $auth_groups
            );

            $auth_group_prompt = $this->anticipate('Which group should the user be attached to?', Arr::flatten($auth_groups));

            // Lookup group based on slug provided in prompt
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_group_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_group) {
            $this->error('Error: No group was found with that slug.');
            $this->error('');
            die();
        }

        // Type
        $type = $this->option('type');
        if($type == null) {

            if($this->confirm('Should this group be associated (by default) with all users that sign in with this authentication provider?')) {
                $type = 'default';
                $meta_key = null;
                $meta_value = null;
            }

            else {
                $type = 'meta';

                // Meta Key
                $meta_key = $this->option('meta_key');
                if($meta_key == null) {
                    $meta_key = $this->ask('What is the key in the provider meta data to match?');
                }

                // Meta Value
                $meta_value = $this->option('meta_value');
                if($meta_value == null) {
                    $meta_value = $this->ask('What is the value for the key in the provider meta data to match?');
                }

            }

        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'auth_provider_id',
                        '['.$auth_provider->short_id.'] '.$auth_provider->slug
                    ],
                    [
                        'auth_group_id',
                        '['.$auth_group->short_id.'] '.$auth_group->slug
                    ],
                    [
                        'type',
                        $type
                    ],
                    [
                        'meta_key',
                        $type == 'meta' ? $meta_key : '(only applicable for meta type)'
                    ],
                    [
                        'meta_value',
                        $type == 'meta' ? $meta_value : '(only applicable for meta type)'
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $authProviderGroupService = new Services\V1\Auth\AuthProviderGroupService();

        // Use service to create record
        $record = $authProviderGroupService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'auth_provider_id' => $auth_provider->id,
            'auth_group_id' => $auth_group->id,
            'type' => $type,
            'meta_key' => $type == 'meta' ? $meta_key : null,
            'meta_value' => $type == 'meta' ? $meta_value : null,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('auth-provider-group:get', [
            'short_id' => $record->short_id
        ]);

    }

}
