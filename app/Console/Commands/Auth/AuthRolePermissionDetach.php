<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class AuthRolePermissionDetach extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-role-permission:detach
                            {short_id? : The short ID of the role.}
                            {--T|auth_tenant_slug= : The slug of the tenant this role belongs to}
                            {--R|auth_role_slug= : The slug of the role}
                            {--P|permission= : The permission in route dot notation (Ex. user.dashboard.index)}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Detach a Permission from an Authentication Role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Role - Detach Permission');

        // Get short ID from console option
        $short_id = $this->argument('short_id');

        // Get auth tenant and role from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');
        $auth_role_slug = $this->option('auth_role_slug');

        // If short ID was specified, lookup by ID
        if($short_id) {
            $auth_role = Models\Auth\AuthRole::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug option was specified, lookup by slug
        elseif($auth_tenant_slug && $auth_role_slug) {
            $auth_role = Models\Auth\AuthRole::query()
                ->whereHas('authTenant', function(Builder $query) use ($auth_tenant_slug) {
                    $query->where('slug', $auth_tenant_slug);
                })
                ->where('slug', $auth_role_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the role belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

            // Get list of roles to show in console
            $auth_roles = Models\Auth\AuthRole::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            // Show table in console output
            $this->table(['Role'], $auth_roles);

            $auth_role_prompt = $this->anticipate('Which role should the permission be detached from?', Arr::flatten($auth_roles));

            // Lookup role based on slug provided in prompt
            $auth_role = Models\Auth\AuthRole::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_role_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_role) {
            $this->error('Error: No role was found.');
            $this->error('');
            die();
        }

        // Loop through array of permissions and format for console table
        $permission_rows = [];
        foreach($auth_role->permissions as $permission) {
            $permission_rows[] = [$permission];
        }

        // Auth Role Permissions
        $this->table(
            ['Permissions'],
            $permission_rows
        );

        // Permission
        $permission = $this->option('permission');
        if($permission == null) {
            $permission = $this->anticipate('What is the permission (route name in dot notation) that should be detached?', $auth_role->permissions);
        }

        // Check if role already has permission attached
        if(!in_array($permission, $auth_role->permissions)) {
            $this->comment('The permission is not attached to this role.');
            $this->comment('Aborting. No changes have been made.');
        } else {
            // Create a variable for the permissions array
            $permissions = $auth_role->permissions;

            // Search the array to get the numeric index of the permission
            $permission_key = array_search($permission, $permissions);

            // Remove the permission from the array
            unset($permissions[$permission_key]);
        }

        // Initialize service
        $authRoleService = new Services\V1\Auth\AuthRoleService();

        // Use service to create record
        $record = $authRoleService->update($auth_role->id, [
            'permissions' => $permissions,
        ]);

        // Show result in console
        $this->comment('Permission successfully detached.');

        // Call the get method to display the tables of values for the record.
        $this->call('auth-role:get', [
            'short_id' => $auth_role->short_id
        ]);

    }

}
