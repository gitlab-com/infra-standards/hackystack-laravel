<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthGroupDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group:delete
                            {short_id? : The short ID of the group.}
                            {--slug= : The slug of the group.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Authentication Group by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->comment('You need to use the `--short_id=a1b2c3d4` or `--slug=example` option.');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_group == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-group:get', [
            'short_id' => $auth_group->short_id,
            '--without-child-relationships' => true
        ]);

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $authGroupService = new Services\V1\Auth\AuthGroupService();

        // Use service to delete record
        $authGroupService->delete($auth_group->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
