<?php

namespace App\Console\Commands\Developer;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;

class GenerateChangelog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'developer:generate-changelog
                            {milestone} : The milestone for this release}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a changelog file for a release with list of merge requests and commit descriptions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->line('');
        $this->info('Generating a Changelog for '.$this->argument('milestone'));

        // Check if API token has been set or return an error
        if(!config('hackystack.developer.gitlab.api_token')) {
            $this->error('You have not set your GitLab API token in the `.env` file.');
            $this->line('1. Visit https://gitlab.com or your private GitLab instance.');
            $this->line('2. Navigate to User Settings > Personal Access Tokens');
            $this->line('3. Create a new token with the `api` scope.');
            $this->line('4. Update the `HS_DEVELOPER_GITLAB_API_TOKEN` variable in the `.env` file');
            $this->line('5. Update the `HS_DEVELOPER_GITLAB_API_USERNAE` variable in the `.env` file.');
            die();
        }

        // Connect to GitLab API using credentials defined in config/hackystack.php
        // and the .env file.
        $gitlab_client = new \Gitlab\Client();
        $gitlab_client->authenticate(config('hackystack.developer.gitlab.api_token'), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $gitlab_client->setUrl(config('hackystack.developer.gitlab.base_url'));
        $gitlab_pager = new \Gitlab\ResultPager($gitlab_client);

        // Get details about the GitLab project
        $project = $gitlab_client->projects()->show(config('hackystack.developer.gitlab.project_id'));

        $this->line('Project Name: '.$project['name_with_namespace']);
        $this->line('Project URL: '.$project['web_url']);
        $this->line('Merge Requests: '.$project['_links']['merge_requests']);
        $this->line('');

        // Lookup milestone to get exact match
        $milestones = $gitlab_client->milestones()->all(config('hackystack.developer.gitlab.project_id'), ['search' => $this->argument('milestone')]);

        // If milestone matched, output details about the milestone
        if(count($milestones) == 1) {

            // Define milestone version and changelog file name based on milestone argument
            $milestone_version = $milestones[0]['title'];
            $changelog_file_name = $milestone_version.'.md';

            // Check if changelog file for this release has already been created.
            if (Storage::disk('changelog')->exists($changelog_file_name)) {
                // Delete existing file since it will be re-generated
                Storage::disk('changelog')->delete($changelog_file_name);
            }

            // Create new changelog file with milestone header
            Storage::disk('changelog')->put($changelog_file_name,'# '.$milestone_version);
            $this->line('The changelog file has been created and can be found changelog/'.$changelog_file_name.'.');

            // Append milestone meta data to changelog file
            Storage::disk('changelog')->append($changelog_file_name,'* **URL:** '.$milestones[0]['web_url']);
            if($milestones[0]['due_date'] != null) {
                Storage::disk('changelog')->append($changelog_file_name,'* **Release Date:** '.$milestones[0]['due_date']);
            }
            Storage::disk('changelog')->append($changelog_file_name,''); // blank line

            if($milestones[0]['description'] != null) {
                Storage::disk('changelog')->append($changelog_file_name,'## Overview');
                Storage::disk('changelog')->append($changelog_file_name,$milestones[0]['description']);
                Storage::disk('changelog')->append($changelog_file_name,''); // blank line
            } else {
                $this->error('Warning: This milestone does not have a description that provides a high-level overview of this release.');
            }

        } else {
            $this->line('');
            $this->error('The milestone passed as an argument did not explicitely match an existing milestone.');

            // Get list of available milestones for quick reference spellcheck.
            $available_milestones = $gitlab_client->milestones()->all(config('hackystack.developer.gitlab.project_id'));
            $this->line('');
            $this->line('This is a list of available milestones for this project:');
            foreach($available_milestones as $available_milestone) {
                $this->line($available_milestone['title']);
            }

            // Additional instructions on accessing milestones
            $this->line('');
            $this->line('You can also visit the GitLab project milestones to make changes.');
            $this->line($project['web_url'].'/-/milestones');

            die();
        }

        //
        // Merge Requests
        //

        $this->line('');
        $this->line('Fetching merge requests...');

        // Get merge requests for GitLab project with specific milestone
        $merge_requests = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'all', [config('hackystack.developer.gitlab.project_id'), [
            'milestone' => $this->argument('milestone'),
            'sort' => 'asc',
            'state' => 'merged',
        ]]);

        // Loop through merge requests and add to array
        $mr_outputs = [];
        foreach($merge_requests as $merge_request) {

            $mr_changelog_label = $this->getMergeRequestCategoryLabel($merge_request['labels']);

            // Add merge request to outputs
            $mr_outputs[] = '`'.$mr_changelog_label.'` '.$merge_request['title'].' - '.$merge_request['references']['short'].' - @'.$merge_request['author']['username'];

        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $mr_collection = collect($mr_outputs);
        $mr_collection_outputs = $mr_collection->sort()->values()->all();

        // Add list of merge requests to changelog file
        Storage::disk('changelog')->append($changelog_file_name,'## Merge Requests ('.$mr_collection->count().')');
        foreach($mr_collection_outputs as $mr_collection_row) {
            Storage::disk('changelog')->append($changelog_file_name,'* '.$mr_collection_row);
        }
        Storage::disk('changelog')->append($changelog_file_name,''); // blank line

        //
        // Commits
        //

        $this->line('Fetching commits...');
        $this->line('');

        // Loop through merge requests
        $commit_outputs = [];
        foreach($merge_requests as $merge_request) {

            $mr_changelog_label = $this->getMergeRequestCategoryLabel($merge_request['labels']);

            // Get commits for specific merge request
            $mr_commits = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'commits', [
                config('hackystack.developer.gitlab.project_id'),
                $merge_request['iid']
            ]);

            // Loop through commits and add values to array
            foreach($mr_commits as $mr_commit) {
                $commit_outputs[] = '`'.$mr_changelog_label.'` '.$mr_commit['title'].' - '.$mr_commit['short_id'].' - !'.$merge_request['iid'];
            }

        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $commit_collection = collect($commit_outputs);
        $commit_collection_outputs = $commit_collection->sort()->values()->all();

        // Add list of merge requests to changelog file
        Storage::disk('changelog')->append($changelog_file_name,'## Commits ('.$commit_collection->count().')');
        foreach($commit_collection_outputs as $commit_collection_row) {
            Storage::disk('changelog')->append($changelog_file_name,'* '.$commit_collection_row);
        }
        Storage::disk('changelog')->append($changelog_file_name,''); // blank line

        $this->line('<fg=green>The changelog file generation has been completed.</>');

    }

    public function getMergeRequestCategoryLabel($merge_request_labels = [])
    {
        // Get label for categorizing changelog entry
        $merge_request_labels = collect($merge_request_labels);
        if($merge_request_labels->count() > 0) {
            // Loop through merge request labels
            foreach($merge_request_labels as $merge_request_label) {
                // If label has scope separator (::)
                if(strpos($merge_request_label, '::')) {
                    // Split string into prefix [0] and suffix [1] using `::` separator
                    $label_parts = explode('::', $merge_request_label);
                    // If label prefix is equal to config variable
                    if($label_parts[0] == config('hackystack.developer.gitlab.changelog_label')) {
                        // Set variable to suffix of label
                        $mr_changelog_label = $label_parts[1];
                    }
                }
            }
        } else {
            $mr_changelog_label = null;
        }

        return $mr_changelog_label;
    }
}
