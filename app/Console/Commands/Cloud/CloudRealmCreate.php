<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudRealmCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-realm:create
                            {--T|auth_tenant_slug= : The slug of the tenant this realm belongs to}
                            {--B|cloud_billing_account_slug= : The slug of the billing account this realm belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider this realm belongs to}
                            {--N|name= : The display name of this realm}
                            {--S|slug= : The alpha-dash shorthand name for this realm}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Cloud Realm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Realms - Create Record');

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this realm belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this realm belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->firstOrFail();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Billing Account
        // --------------------------------------------------------------------
        //

        // Get billing account slug
        $cloud_billing_account_slug = $this->option('cloud_billing_account_slug');

        // If billing account slug option was specified, lookup by slug
        if($cloud_billing_account_slug) {
            $billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_billing_account_slug)
                ->first();
        }

        // If billing account slug was not provided, prompt for input
        else {

            // Get list of billing accounts to show in console
            $cloud_billing_accounts = Models\Cloud\CloudBillingAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->get(['slug'])
                ->toArray();

            // If no billing accounts exist, then return an error
            if(count($cloud_billing_accounts) == 0) {
                $this->error('Error: No billing accounts were found for the tenant.');
                $this->comment('Please create a billing account before creating a realm.');
                $this->line('cloud-billing-account:create');
                $this->error('');
                die();
            }

            $this->line('');
            $this->line('Available billing accounts: '.implode(',', Arr::flatten($cloud_billing_accounts)));

            $cloud_billing_account_prompt = $this->anticipate('Which billing account should this realm belong to?', Arr::flatten($cloud_billing_accounts));

            // Lookup billing account based on slug provided in prompt
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_billing_account_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_billing_account) {
            $this->error('Error: No billing account was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this cloud realm?');
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this cloud realm?');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'cloud_billing_account_id',
                        '['.$cloud_billing_account->short_id.'] '.$cloud_billing_account->slug
                    ],
                    [
                        'cloud_provider_id',
                        '['.$cloud_provider->short_id.'] '.$cloud_provider->slug
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'slug',
                        $slug
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $cloudRealmService = new Services\V1\Cloud\CloudRealmService();

        // Use service to create record
        $record = $cloudRealmService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_billing_account_id' => $cloud_billing_account->id,
            'cloud_provider_id' => $cloud_provider->id,
            'name' => $name,
            'slug' => $slug,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-realm:get', [
            'short_id' => $record->short_id
        ]);

    }

}
