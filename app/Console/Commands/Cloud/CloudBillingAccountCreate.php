<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudBillingAccountCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-billing-account:create
                            {--T|auth_tenant_slug= : The slug of the tenant this billing account belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider this billing account belongs to}
                            {--N|name= : The display name of this billing account}
                            {--S|slug= : The alpha-dash shorthand name for this billing account}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Cloud Billing Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Billing Accounts - Create Record');

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this billing account belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this billing account belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this cloud billing account?');
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this cloud billing account?');
        }

        // GCP Cloud Provider
        if($cloud_provider->type == 'gcp') {

            // Check if Cloud Provider API credentials (JSON service account) has been configured
            if($cloud_provider->api_credentials == null) {

                $this->line('The cloud provider credentials have not been configured so a GCP billing account cannot be configured yet.');
                $this->line('You can set the cloud provider credentials using `cloud-provider:credentials '.$cloud_provider->short_id.'`');
                $this->line('You can update the GCP billing account after it has been created using `cloud-billing-account:edit a1b2c3d4');

            } else {

                // Ininitalize service method for GCP Organization Billing Accounts
                $cloud_billing_organization_service = new Services\V1\Vendor\Gcp\CloudBillingOrganizationService($cloud_provider->id);

                // Get list of available billing accounts for the organization
                $gcp_billing_accounts = $cloud_billing_organization_service->list();

                // Loop through billing accounts and add values to array
                $gcp_billing_account_rows = [];
                $gcp_billing_account_autocomplete = [];
                foreach($gcp_billing_accounts as $gcp_billing_account) {

                    // Add record to table output row
                    $gcp_billing_account_rows[] = [
                        'billing_account_id' => $gcp_billing_account['name'],
                        'display_name' => $gcp_billing_account['displayName'],
                    ];

                    // Add record to array for tab completion in console prompt
                    $gcp_billing_account_autocomplete[] = $gcp_billing_account['name'];

                }

                // Prompt the user for the new billing account ID or use the current billing account meta data
                if($this->confirm('No GCP billing account has been selected. Do you want to choose a billing account now?')) {

                    // Show table in console output with list of GCP billing accounts
                    $headers = ['Billing Account ID', 'Display Name'];
                    $this->table($headers, $gcp_billing_account_rows);
                    $this->comment('Total: '.count($gcp_billing_account_rows));
                    $this->comment('');

                    $gcp_billing_account_id = $this->choice('What is the billing account ID for the billing account that should be used?', $gcp_billing_account_autocomplete, null);

                    // Use API to get GCP Billing Account details
                    $api_meta_data = $cloud_billing_organization_service->get(['billing_account_name' => $gcp_billing_account_id]);

                } else {
                    $api_meta_data = null;
                }

            }

        }

        // AWS Cloud Provider
        elseif($cloud_provider->type == 'aws') {

            // There is no configuration of a billing account with AWS so we
            // use the existing value (likely a null value)
            $api_meta_data = null;

        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'cloud_provider_id',
                        '['.$cloud_provider->short_id.'] '.$cloud_provider->slug
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'slug',
                        $slug
                    ],
                ]
            );

            if($api_meta_data != null) {

                $api_meta_data_values = [];
                foreach($api_meta_data as $meta_data_key => $meta_data_value) {
                    $api_meta_data_values[] = [
                        $meta_data_key,
                        is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
                    ];
                }

                $this->comment('API Meta Data');
                $this->table(
                    ['API Meta Data Column', 'API Value'],
                    $api_meta_data_values
                );

            }

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $cloudBillingAccountService = new Services\V1\Cloud\CloudBillingAccountService();

        // Use service to create record
        $record = $cloudBillingAccountService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_provider_id' => $cloud_provider->id,
            'name' => $name,
            'slug' => $slug,
            'api_meta_data' => $api_meta_data
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-billing-account:get', [
            'short_id' => $record->short_id
        ]);

    }

}
