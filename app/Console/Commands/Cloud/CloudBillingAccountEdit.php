<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use App\Services\V1\Vendor\Gcp\CloudBillingOrganizationService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudBillingAccountEdit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-billing-account:edit
                            {short_id? : The short ID of the billing account.}
                            {--N|name= : The new display name of this billing account}
                            {--S|slug= : The new alpha-dash shorthand name for this billing account}
                            {--gcp_billing_account_id= : The new alphadash ID of the GCP Billing Account (Ex. A1B2C3-D4E5F6-A7B8C9)}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update a Cloud Billing Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Billing Accounts - Edit Record');

        // Get arguments and options
        $short_id = $this->argument('short_id');
        $name = $this->option('name');
        $slug = $this->option('slug');
        $gcp_billing_account_id = $this->option('gcp_billing_account_id');

        // If short ID was specified, lookup by ID
        if($short_id) {
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If short ID or slug is not set, return an error message
        else {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of billing accounts using `cloud-billing-account:list`');
            $this->line('You can lookup by short ID using `cloud-billing-account:get a1b2c3d4`');
            $this->line('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');
        if($name == null) {
            if($this->confirm('The current name of this billing account is `'.$cloud_billing_account->name.'`. Do you want to change the name?')) {
                // Prompt the user for the new billing account name
                $name = $this->ask('What is the new display name of this cloud billing account?');
            } else {
                // Use the current billing account name
                $name = $cloud_billing_account->name;
            }
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            if($this->confirm('The current slug of this billing account is `'.$cloud_billing_account->slug.'`. Do you want to change the slug?')) {
                // Prompt the user for the new billing account slug
                $slug = $this->ask('What is the new alpha-dash shorthand name for this cloud billing account?');
            } else {
                // Use the current billing account slug
                $slug = $cloud_billing_account->slug;
            }
        }

        // GCP Cloud Provider
        if($cloud_billing_account->cloudProvider->type == 'gcp') {

            // Check if Cloud Provider API credentials (JSON service account) has been configured
            if($cloud_billing_account->cloudProvider->api_credentials == null) {

                $this->line('The cloud provider credentials have not been configured so a billing account cannot be configured yet.');
                $this->line('You can set the cloud provider credentials using `cloud-provider:credentials '.$cloud_billing_account->cloudProvider->short_id.'`');

            } else {

                // Ininitalize service method for GCP Organization Billing Accounts
                $cloud_billing_organization_service = new Services\V1\Vendor\Gcp\CloudBillingOrganizationService($cloud_billing_account->cloud_provider_id);

                // Get list of available billing accounts for the organization
                $gcp_billing_accounts = $cloud_billing_organization_service->list();

                // Loop through billing accounts and add values to array
                $gcp_billing_account_rows = [];
                $gcp_billing_account_autocomplete = [];
                foreach($gcp_billing_accounts as $gcp_billing_account) {

                    // Add record to table output row
                    $gcp_billing_account_rows[] = [
                        'billing_account_id' => $gcp_billing_account['name'],
                        'display_name' => $gcp_billing_account['displayName'],
                    ];

                    // Add record to array for tab completion in console prompt
                    $gcp_billing_account_autocomplete[] = $gcp_billing_account['name'];

                }

                // Check if API meta data has been populated
                //if($cloud_billing_account->api_meta_data != null && array_key_exists('name', $cloud_billing_account->api_meta_data)) {
                    //$prompt_message = 'The current GCP billing account ID is `'.$cloud_billing_account->api_meta_data['name'].'`. Do you want to change to a different billing account?';
                //} else {
                    $prompt_message = 'No GCP billing account has been selected. Do you want to choose a billing account now?';
                //}

                // Prompt the user for the new billing account ID or use the current billing account meta data
                if($this->confirm($prompt_message)) {

                    // Show table in console output with list of GCP billing accounts
                    $headers = ['Billing Account ID', 'Display Name'];
                    $this->table($headers, $gcp_billing_account_rows);
                    $this->comment('Total: '.count($gcp_billing_account_rows));
                    $this->comment('');

                    $gcp_billing_account_id = $this->choice('What is the billing account ID for the billing account that should be used?', $gcp_billing_account_autocomplete, null);

                    // Use API to get GCP Billing Account details
                    $api_meta_data = $cloud_billing_organization_service->get(['billing_account_name' => $gcp_billing_account_id]);

                } else {
                    $api_meta_data = $cloud_billing_account->api_meta_data;
                }

            }

        }

        // AWS Cloud Provider
        elseif($cloud_billing_account->cloudProvider->type == 'aws') {

            // There is no configuration of a billing account with AWS so we
            // use the existing value (likely a null value)
            $api_meta_data = $cloud_billing_account->api_meta_data;

        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            $this->comment('Cloud Billing Account');

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'id',
                        $cloud_billing_account->id
                    ],
                    [
                        'short_id',
                        $cloud_billing_account->short_id
                    ],
                    [
                        '[relationship] authTenant',
                        '['.$cloud_billing_account->authTenant->short_id.'] '.$cloud_billing_account->authTenant->slug
                    ],
                    [
                        '[relationship] cloudProvider',
                        '['.$cloud_billing_account->cloudProvider->short_id.'] '.$cloud_billing_account->cloudProvider->slug
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'slug',
                        $slug
                    ],
                ]
            );

            if($api_meta_data != null) {

                $api_meta_data_values = [];
                foreach($api_meta_data as $meta_data_key => $meta_data_value) {
                    $api_meta_data_values[] = [
                        $meta_data_key,
                        is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
                    ];
                }

                $this->comment('API Meta Data');
                $this->table(
                    ['API Meta Data Column', 'API Value'],
                    $api_meta_data_values
                );

            }

            // Ask for confirmation to abort update.
            if($this->confirm('Do you want to abort the update of the record?')) {
                $this->error('Error: You aborted. The record was not updated.');
                die();
            }

        }

        // Initialize service
        $cloudBillingAccountService = new Services\V1\Cloud\CloudBillingAccountService();

        // Use service to create record
        $record = $cloudBillingAccountService->update($cloud_billing_account->id, [
            'name' => $name,
            'slug' => $slug,
            'api_meta_data' => $api_meta_data,
        ]);

        // Show result in console
        $this->comment('Record updated successfully.');

    }

}
