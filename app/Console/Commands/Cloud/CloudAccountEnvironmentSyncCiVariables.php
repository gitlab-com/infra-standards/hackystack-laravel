<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CloudAccountEnvironmentSyncCiVariables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment:sync-ci-variables
                            {short_id? : The short ID of the environment.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get updated CI variables from Git instance for a Cloud Account Environment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of accounts using `cloud-account-environment:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_environment = Models\Cloud\CloudAccountEnvironment::with([
                    'authTenant',
                    'cloudProvider',
                    // 'cloudAccountEnvironments'
                ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account_environment == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account Values
        $this->comment('');
        $this->comment('Cloud Account Environment');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account_environment->id
                ],
                [
                    'short_id',
                    $cloud_account_environment->short_id
                ],
                [
                    'auth_tenant_id',
                    '['.$cloud_account_environment->authTenant->short_id.'] '.$cloud_account_environment->authTenant->slug
                ],
                [
                    'cloud_provider_id',
                    '['.$cloud_account_environment->cloudProvider->short_id.'] '.$cloud_account_environment->cloudProvider->slug
                ],
                [
                    'cloud_account_environment_template_id',
                    '['.$cloud_account_environment->cloudAccountEnvironmentTemplate->short_id.'] '.$cloud_account_environment->cloudAccountEnvironmentTemplate->rendered_name
                ],
                [
                    'cloud_account_id',
                    '['.$cloud_account_environment->cloudAccount->short_id.'] '.$cloud_account_environment->cloudAccount->slug
                ],
                [
                    'name',
                    $cloud_account_environment->name
                ],
                [
                    'description',
                    $cloud_account_environment->description
                ],
                [
                    'state',
                    $cloud_account_environment->state
                ]
            ]
        );

        if($cloud_account_environment->git_meta_data != null) {

            $git_meta_data_values = [];
            foreach($cloud_account_environment->git_meta_data as $meta_data_key => $meta_data_value) {
                $git_meta_data_values[] = [
                    $meta_data_key,
                    Str::limit(is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value, 100, '...')
                ];
            }

            $this->newLine();
            $this->table(
                ['Git Meta Data Column', 'API Value'],
                $git_meta_data_values
            );
            $this->newLine();

            // Use service method to get list of CI variables on Git instance
            $cloudAccountEnvironmentService = new \App\Services\V1\Cloud\CloudAccountEnvironmentService();
            $cloud_account_environment->git_ci_variables = $cloudAccountEnvironmentService->listGitCiVariables($cloud_account_environment->id);
            $cloud_account_environment->save();

            $this->comment('CI Variables');
            $this->table(
                ['key', 'value', 'variable_type', 'protected', 'masked', 'environment_scope'],
                $cloud_account_environment->git_ci_variables
            );
            $this->newLine();


        } else {

            $this->newLine();
            $this->line('<fg=red>Git Meta Data is empty. Check logs for errors with the CloudAccountEnvironment::updateGitMetaData method</>');
            $this->newLine();

        }

    }
}
