<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudAccountEnvironmentTemplateCreateCiVariable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:create-ci-variable
                            {short_id? : The short ID of the environment template.}
                            {--key= : The CI variable key (Ex. MY_ENVIRONMENT_VARIABLE)}
                            {--dynamic_type= : The type of value when generating a CI variable value. (See docs)}
                            {--value= : The CI variable string value or dynamic type value (Ex. cool-value -or- projectId)}
                            {--variable_type= : (Optional) The CI variable type (env_var|file) to set. (Default env_var)}
                            {--protected : (Optional) If set, protected variable flag will be set to true. (Default false)}
                            {--masked : (Optional) If set, masked variable flag will be set to true. (Default false)}
                            {--environment_scope= : (Optional) If set, this variable will only apply to specific environments. (Default *)}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a CI Variable for a Cloud Account Environment Template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->comment('');
        $this->comment('Cloud Account Environment Template - Create CI Variable');

        // If short ID was specified, lookup by ID
        if ($this->argument('short_id')) {
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('short_id', $this->argument('short_id'))
                ->firstOrFail();
        }

        // If short ID or slug is not set, return an error message
        else {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of environment templates using `cloud-account-environment-template:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment-template:create-ci-variable a1b2c3d4`');
            $this->line('');
            die();
        }

        // Define array of value types
        $dynamic_types = [
            'string',
            'auth-group-namespace-slug',
            'auth-user-string',
            'auth-user-provider-meta-data',
            'cloud-account-string',
            'cloud-account-api-meta-data',
            'cloud-account-git-meta-data',
            'cloud-account-dns-meta-data',
            'cloud-account-environment-string',
            'cloud-account-environment-env-prefix-string',
            'cloud-account-environment-git-meta-data',
            'cloud-organization-unit-string',
            'cloud-provider-string',
            'cloud-realm-string',
        ];

        // Check if key option is set
        if ($this->option('key')) {
            $key = $this->option('key');
        } else {
            $key = $this->ask('What is the name of the key (name) for this CI variable (Ex. MY_ENVIRONMENT_VARIABLE)');
        }

        if ($this->option('dynamic_type')) {
            if (in_array($this->option('dynamic_type'), $dynamic_types)) {
                $dynamic_type = $this->option('dynamic_type');
            } else {
                $this->error('You did not specify a valid value for `--dynamic_types`.');
                die();
            }
        } else {
            $dynamic_type = $this->choice(
                'What type of value should be generated for this key when an Environment uses this template?',
                $dynamic_types,
                0
            );
        }

        if ($this->option('value')) {
            $value = $this->option('value');
        } else {
            if ($dynamic_type == 'string') {
                $value = $this->ask('What is the static string value for this CI variable? (Ex. my-cool-string)');
            } elseif ($dynamic_type == 'auth-group-namespace-slug') {
                $value = $this->ask('What is the namespace of the AuthGroup the user is a member of that we should use the slug for? (Ex. department)');
            } elseif ($dynamic_type == 'auth-user-string') {
                $value = $this->ask('What column in the AuthUser model should we use as the value for this CI variable?');
            } elseif ($dynamic_type == 'auth-user-provider-meta-data') {
                $value = $this->ask('What array key in the AuthUser model provider_meta_data array should we use for the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-string') {
                $value = $this->ask('What column in the CloudAccount model should we use as the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-api-meta-data') {
                $value = $this->ask('What array key in the CloudAccount model api_meta_data array should we use for the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-git-meta-data') {
                $value = $this->ask('What array key in the CloudAccount model git_meta_data array should we use for the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-dns-meta-data') {
                $value = $this->ask('What array key in the CloudAccount model dns_meta_data array should we use for the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-environment-string') {
                $value = $this->ask('What column in the CloudAccountEnvironment model should we use as the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-environment-env-prefix-string') {
                $value = $this->ask('What column in the CloudAccountEnvironment model should we use as the value (with an `env-` prefix) for this CI variable?');
            } elseif ($dynamic_type == 'cloud-account-environment-git-meta-data') {
                $value = $this->ask('What array key in the CloudAccountEnvironment model git_meta_data array should we use for the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-organization-unit-string') {
                $value = $this->ask('What column in the CloudOrganizationUnit model should we use as the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-provider-string') {
                $value = $this->ask('What column in the CloudProvider model should we use as the value for this CI variable?');
            } elseif ($dynamic_type == 'cloud-realm-string') {
                $value = $this->ask('What column in the CloudRealm model should we use as the value for this CI variable?');
            }
        }

        // Use variable_type if set
        if ($this->option('variable_type')) {
            $variable_type = $this->option('variable_type');
        } else {
            $variable_type = 'env_var';
        }

        // If protected flag is set, set value to true.
        if ($this->option('protected')) {
            $protected = 'true';
            $this->line('<fg=yellow>The --protected flag has been set, so the CI variable will be only be available to pipelines running on protected branches and tags.');
        } else {
            $protected = 'false';
        }

        // If masked flag is set, set value to true.
        if ($this->option('masked')) {
            $masked = 'true';
            $this->line('<fg=yellow>The --masked flag has been set, so the CI variable will be only be masked from CI outputs. This can have an adverse affect on Terraform commands in some cases.');
        } else {
            $masked = 'false';
        }

        // If masked flag is set, set value to true.
        if ($this->option('environment_scope')) {
            $environment_scope = $this->option('environment_scope');
            $this->line('<fg=yellow>The --environment_scope flag has been set to `'.$environment_scope.'`, so the CI variable will be only be available to environments that match this criteria.');
        } else {
            $environment_scope = '*';
        }

        // If no interaction flag is not set, show preview output
        if ($this->option('no-interaction') == false) {
            $this->comment('Cloud Account Environment Template');

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'template',
                        '['.$cloud_account_environment_template->short_id.'] '.$cloud_account_environment_template->rendered_name
                    ],
                    [
                        'key',
                        $key
                    ],
                    [
                        'dynamic_type',
                        $dynamic_type
                    ],
                    [
                        'value',
                        $value
                    ],
                    [
                        'variable_type',
                        $variable_type.' (Use --variable_type option to change)'
                    ],
                    [
                        'protected',
                        $protected.' (Use --protected flag to change)',
                    ],
                    [
                        'masked',
                        $masked.' (Use --masked flag to change)',
                    ],
                    [
                        'environment_scope',
                        $environment_scope.' (Use --environment_scope option to change)',
                    ],
                ]
            );

            // Ask for confirmation to abort update.
            if ($this->confirm('Do you want to abort the creation of the CI variable to the template?')) {
                $this->error('Error: You aborted. The template was not updated.');
                die();
            }
        }

        // Define array for CI variable to be merged into database array
        $ci_variable_array = [
            $key => [
                'key' => $key,
                'dynamic_type' => $dynamic_type,
                'value' => $value,
                'variable_type' => $variable_type,
                'protected' => $protected,
                'masked' => $masked,
                'environment_scope' => $environment_scope
            ],
        ];

        // Define array using existing database values and merge new CI variable into array
        $existing_array = $cloud_account_environment_template->git_ci_variables ? $cloud_account_environment_template->git_ci_variables : array();
        $updated_array = array_merge($existing_array, $ci_variable_array);

        // Update database array
        $cloud_account_environment_template->git_ci_variables = $updated_array;
        $cloud_account_environment_template->save();

        $this->comment('Record updated successfully.');
    }
}
