<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class CloudAccountEnvironmentTemplateList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:list
                            {--T|auth_tenant_slug= : The slug of the tenant that the environment templates belong to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that environment templates belongs to}
                            {--search=* : Wildcard search of name or slug.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Account Environment Templates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get cloud provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Cloud\CloudAccountEnvironmentTemplate::query()
            ->withCount(['cloudAccountEnvironments'])
            ->with([
                'authTenant',
                'cloudProvider'
            ])
            ->where(function ($query) use ($auth_tenant_slug) {
                if ($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function ($query) use ($cloud_provider_slug) {
                if ($cloud_provider_slug) {
                    $query->whereHas('cloudProvider', function (Builder $query) use ($cloud_provider_slug) {
                        $query->where('slug', $cloud_provider_slug);
                    });
                }
            })->where(function ($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if (count($search_array) > 0) {
                    foreach ($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                    }
                }
            })->orderBy('display_order')
            ->orderBy('name')
            ->orderBy('git_name')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach ($records as $template_record) {
            $template_rows[] = [
                'short_id' => $template_record->short_id,
                'auth_tenant' => '['.$template_record->authTenant->short_id.'] '.$template_record->authTenant->slug,
                'cloud_provider' => '['.$template_record->cloudProvider->short_id.'] '.$template_record->cloudProvider->slug,
                'environment_count' => $template_record->cloud_account_environments_count,
                'git_project_id' => $template_record->git_project_id,
                'name' => $template_record->rendered_name,
                'description' => Str::limit(($template_record->rendered_description), 30, '...'),
                'state' => $template_record->state,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Environment Templates - List of Records');

        if (count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Environment Count', 'Git Project ID', 'Name', 'Description', 'State'];
            $this->table($headers, $template_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');
        } else {
            $this->line('No records exist.');
            $this->comment('');
        }
    }
}
