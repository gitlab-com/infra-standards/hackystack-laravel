<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class HackystackInstallDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hackystack:install-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform database migration and seed of default data. This should be run after `hackstack:install`.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Seeding MySQL database                                                        </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');
        // Create query to database to verify that connection exists
        try {
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $existing_database = DB::select($query, [config('database.connections.mysql.host')]);
            $this->line('<fg=green>Database connection successful.</>');
            $this->line('');
        } catch(\Illuminate\Database\QueryException $e) {
            $this->line('<fg=red>'.$e->getMessage().'</>');
            $this->line('');
            $this->error('Installation aborted. Please configure the database connection in the `.env` file and run the command again.');
            $this->line('');
            die();
        }

        // Running database migrations
        $this->line('Running database migrations to create table schema');
        $this->call('migrate', []);
        $this->line('');

        // Seed the database with defaults
        $this->line('Seeding database with authentication defaults');
        $this->call('db:seed', [
            '--class' => 'AuthBaseSeed'
        ]);
        $this->line('');

        // Get local provider from database
        $local_provider = Models\Auth\AuthProvider::query()
            ->where('slug', 'local')
            ->firstOrFail();

        // Initialize auth provider service
        $authProviderService = new Services\V1\Auth\AuthProviderService();

        // Update the local provider with the enabled/disabled state
        $authProviderService->update($local_provider->id, [
            'flag_enabled' => config('hackystack.auth.local.enabled') ? 1 : 0
        ]);

        // Confirmation message
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=green;options=bold>HackyStack database configuration complete.</>');
        $this->line('');

    }
}
