<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CloudController extends BaseController
{
    public function __construct()
    {
        // TODO Additional validation of roles and permissions
    }

    public function index(Request $request)
    {

        $auth_user_id = $request->user()->id;

        $cloud_providers = Models\Cloud\CloudProvider::query()
            ->with([
                'cloudAccounts' => function($query) use($auth_user_id) {
                    $query->whereHas('cloudAccountUsers', function($query) use($auth_user_id) {
                        $query->where('auth_user_id', $auth_user_id);
                    });
                }
            ])
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->get();

            /*
        $cloud_realms = Models\Cloud\CloudRealm::query()
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->get();

        $cloud_organization_units = Models\Cloud\CloudOrganizationUnit::query()
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->get();

        $cloud_accounts = Models\Cloud\CloudAccount::query()
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->get();
            */

        return view('user.cloud.index', compact([
            'request',
            'cloud_providers',
            //'cloud_realms',
            //'cloud_organization_units',
            //'cloud_accounts'
        ]));

    }

}
