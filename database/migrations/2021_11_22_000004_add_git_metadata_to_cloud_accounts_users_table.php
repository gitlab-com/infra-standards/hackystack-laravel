<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGitMetadataToCloudAccountsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cloud_accounts_users', 'git_meta_data')) {
            return;
        }

        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->json('git_meta_data')->nullable()->after('api_meta_data');
            $table->timestamp('git_group_user_provisioned_at')->nullable()->after('provisioned_at');
            $table->boolean('flag_git_group_user_provisioned')->default(false)->nullable()->after('flag_provisioned');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->dropColumn('git_meta_data');
            $table->dropColumn('git_group_user_provisioned_at');
            $table->dropColumn('flag_git_group_user_provisioned');
        });
    }
}
