<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiMetaDataToCloudAccountsUsersRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('api_meta_data', 'cloud_accounts_users_roles')) {
            return;
        }

        Schema::table('cloud_accounts_users_roles', function (Blueprint $table) {
            $table->json('api_meta_data')->nullable()->after('cloud_realm_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_users_roles', function (Blueprint $table) {
            $table->dropColumn('api_meta_data');
        });
    }
}
