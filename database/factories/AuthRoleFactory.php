<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Models\Auth\AuthRole::class, function (Faker $faker) {

    $name = $faker->words(3, true);

    return [
        'auth_tenant_id' => factory(\App\Models\Auth\Tenant::class)->create();
        'name' => $name,
        'slug' => Str::limit(Str::kebab($name), 30, ''),
        'description' => $faker->paragraph(2, true),
        'permissions' => json_encode([
            'auth' => [
                'admin'
            ],
            'cloud' => [
                'admin'
            ]
        ]),
        'flag_is_enabled_by_default' => $faker->numberBetween(0, 1),
        'flag_is_tenant_role' => $faker->numberBetween(0, 1),
    ];
});
