<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [

        'glamstack-okta-sdk-prod' => [
            'name' => 'glamstack-okta-sdk-prod',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/glamstack-okta-sdk-prod.log'),
        ],

        'glamstack-okta-sdk-preview' => [
            'name' => 'glamstack-okta-sdk-preview',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/glamstack-okta-sdk-preview.log'),
        ],

        'glamstack-okta-sdk-dev' => [
            'name' => 'glamstack-okta-sdk-dev',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/glamstack-okta-sdk-dev.log'),
        ],

        'glamstack-gitlab-sdk' => [
            'name' => 'glamstack-gitlab-sdk',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/glamstack-gitlab-sdk.log'),
        ],

        'glamstack-google-auth-sdk' => [
            'name' => 'glamstack-google-auth-sdk',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/glamstack-google-auth-sdk.log'),
        ],

        'hackystack-auth' => [
            'name' => 'hackystack-auth',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/hackystack-auth.log'),
        ],

        'hackystack-cloud' => [
            'name' => 'hackystack-cloud',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/hackystack-cloud.log'),
        ],

        'hackystack-git' => [
            'name' => 'hackystack-git',
            'driver' => 'single',
            'level' => 'debug',
            'path' => storage_path('logs/hackystack-git.log'),
        ],

        'stack' => [
            'driver' => 'stack',
            'channels' => ['single', 'bugsnag', 'sentry'],
            'ignore_exceptions' => false,
        ],

        'bugsnag' => [
            'driver' => 'bugsnag',
        ],

        'sentry' => [
            'driver' => 'sentry',
            'level' => env('LOG_LEVEL', 'error'),
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],
    ],

];
