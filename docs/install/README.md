# Installation Documentation

[[_TOC_]]

## Pre-Requisites

### MacOS Packages

```
brew update

# MySQL Database
brew install mysql@5.7
brew services start mysql@5.7

# PHP
brew install php@7.4
brew services start php@7.4

# Web Server
brew install nginx
brew services start nginx

# PHP Package Manager
brew install composer

# Javascript Package Manager
brew install npm

# PHP Laravel Web Server Helper
composer global require laravel/valet
valet install
valet tld valet
cd ~/Sites && valet park

# Git Client
brew install git
```

### Linux Packages

TODO

### Ansible Role

TODO

## Create a Database

1. Use your preferred MySQL client to connect to your database server. You can host a database on your machine, a dedicated server, or use a cloud managed service (ex. AWS RDS, GCP CloudSQL).

2. Create a new database with `utf8mb4` encoding and `utf8mb4_unicode_ci` collation. You can name it whatever you'd like (ex. `hackystack_db`).

3. Create a new database user and password with strong credentials. You should never use your `root` account for applications.

4. Grant the new database user access to the database you created.

5. Save the credentials for your database in a safe place (ex. password manager).

## Installing HackyStack

### MacOS and Linux Environment

1. Change to the directory where your web applications are hosted.

    ```
    # MacOS: Change to the directory where you keep your web applications (may vary)
    cd ~/Sites

    # Linux: Change to the directory where web applications are hosted on your server
    cd /srv/www
    ```

2. Clone the HackyStack Portal Git repo

    ```
    git clone https://gitlab.com/hackystack/hackystack-portal.git
    ```

3. Change to the sub-directory for the HackyStack Portal

    ```
    cd hackystack-portal
    ```

4. Download the dependencies using Composer and NPM/Yarn.

    ```
    composer install

    npm install
    ```

4. Run the installation script to configure the environment variables

    ```
    php hacky hackystack:install
    ```

## Configuring HackyStack

1. Run database migrations and seed with system defaults

    ```
    php hacky hackystack:install-database
    ```

2. Create a local administrator account

    ```
    # Create a user account for yourself
    php hacky auth-user:create

    # Attach your user to the administrators group
    php hacky auth-group-user:create --auth_group_slug=auth-super-admin

    # See a list of all available groups that other users can be attached to
    php hacky auth-group:list
    ```

3. Optional: Configure an SSO provider

    ```
    # Get a list of SSO providers that you can configure
    php hacky auth-provider:list

    # Configure an SSO provider using the ID provided in the list
    php hacky auth-provider:edit {a1b2c3d4}
    ```

4. Rename the default tenant and configure the Git API

    ```
    php hacky auth-tenant:edit --auth_tenant_slug=default
    ```

    > If you change the slug, any documentation references to the `default` slug need to be replaced with your slug.

5. Configure the cloud provider(s)

    ```
    # Get a list of cloud providers that can be configured
    php hacky cloud-provider:list

    # Configure the credentials for a cloud provider using the ID provided in the list
    php hacky cloud-provider:edit {a1b2c3d4}
    ```

6. You're all set. You can use the CLI to administer HackyStack or visit the web UI to use HackyStack's features.

    ```
    php hacky
    ```

    > If you are using MacOS and installed `valet`, you should be able to visit [http://hackystack-portal.valet](http://hackystack-portal.valet). Valet provides an overlay of the `http://localhost` functionality that has a mock-DNS feature that uses the directory name of the Laravel PHP application. We configured the `.valet` top-level domain (TLD) above in the pre-requisites, however you can change this to anything you'd like (`.app`, `.dev`, `.local`, etc).

## Importing Data

###

1. Copy the import template.

    ```
    # Authentication Groups
    cp config/hackystack-auth-groups.php.example config/hackystack-auth-groups.php

    # Authentication Provider Fields
    cp config/hackystack-auth-provider-fields.php.example config/hackystack-auth-provider-fields.php

    # Cloud Realms, Organization Units, and Accounts
    cp config/hackystack-cloud-realms.php.example config/hackystack-cloud-realms.php
    ```

2. Open the new file and read the instructions at the top.

3. Run the import CLI command to import your configuration.

    ```
    # Authentication Groups
    php hacky auth-group:import-config-file

    # Authentication Provider Fields
    php hacky auth-provider-field:import-config-file

    # Cloud Realms, Organization Units, and Accounts
    php hacky cloud-realm:import-config-file
    ```

## Reinstalling HackyStack

1. Back up your database if you want to preserve any records.

2. Run the CLI command to perform a backup of your configuration files and reset the environment configuration. You will be prompted before the database data is destroyed.

    ```
    php hacky hackystack:reset
    ```

    > The `.env` and `config/hackystack.php` files are backed up to the `storage/backup` directory. This directory is included in the `.gitignore` so you don't accidentally commit sensitive data to your source control repository.

3. Reinstall with new or previous environment variables.

    ```
    # Option A - New Environment Variables

    # Run the installation script to configure the environment variables
    php hacky hackystack:install
    ```

    ```
    # Option B - Previous Environment Variables

    # Copy the environment variables file backup to the current environment
    cp storage/backup/env/.env.backup-{timestamp} .env

    # Optional: Copy the config/hackystack.php file backup to the current configuration
    cp storage/backup/config/hackystack.php.backup-{timestamp} config/hackystack.php
    ```

## Example Automation for HackyStack Installation

You can use the flags available with each of the `php hacky` commands to run a command without being prompted for input.

```
cd ~/Sites/hackystack/hackystack-portal
php hacky hackystack:reset -D
cp .env.dev .env
php hacky hackystack:install-database
php hacky auth-tenant:edit -n --auth_tenant_slug=default --git_provider_type=gitlab --git_provider_base_url="https://gitlab.com" --git_provider_base_group_id={XXXXX} --git_provider_api_token={XXXXXXXXXXXXXXXXXXXX}
php hacky auth-group:import-config-file -n --auth_tenant_slug=default
php hacky auth-provider:edit --auth_tenant_slug=default --auth_provider_slug=okta --base_url="https://{company}.okta.com" --client_id={XXXXXXXXXXXXXXXXXXXX} --client_secret={XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX} --flag_enabled=1
php hacky cloud-provider:credential --auth_tenant_slug=default --cloud_provider_slug=gcp --gcp_credentials_file={filename}.json
php hacky cloud-provider:credential --auth_tenant_slug=default --cloud_provider_slug=aws --aws_access_key_id={XXXXXXXXXXXXXXXXXXXX} --aws_access_key_secret="{XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}"
php hacky cloud-realm:import-config-file --auth_tenant_slug=default --cloud_billing_account_slug=default --cloud_provider_slug=aws -n
php hacky cloud-realm:import-config-file --auth_tenant_slug=default --cloud_billing_account_slug=default --cloud_provider_slug=gcp -n
```

You can also create a function using Bash, zsh, etc to run a single command (ex. `hacky-rebuild` to recreate your environment).

```
~/.bash_profile

hacky-rebuild() { cd ~/Sites/hackystack/hackystack-portal && php hacky hackystack:reset -D && cp .env.dev .env && php hacky hackystack:install-database && php hacky auth-tenant:edit -n --auth_tenant_slug=default --git_provider_type=gitlab --git_provider_base_url="https://gitlab.com" --git_provider_base_group_id={XXXXX} --git_provider_api_token={XXXXXXXXXXXXXXXXXXXX} && php hacky auth-group:import-config-file -n --auth_tenant_slug=default && php hacky auth-provider:edit --auth_tenant_slug=default --auth_provider_slug=okta --base_url="https://{company}.okta.com" --client_id={XXXXXXXXXXXXXXXXXXXX} --client_secret={XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX} --flag_enabled=1 && php hacky cloud-realm:import-config-file --auth_tenant_slug=default --cloud_billing_account_slug=default --cloud_provider_slug=aws -n && php hacky cloud-provider:credential --auth_tenant_slug=default --cloud_provider_slug=gcp --gcp_credentials_file={filename}.json && php hacky cloud-provider:credential --auth_tenant_slug=default --cloud_provider_slug=aws --aws_access_key_id={XXXXXXXXXXXXXXXXXXXX} --aws_access_key_secret="{XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}" && php hacky cloud-realm:import-config-file --auth_tenant_slug=default --cloud_billing_account_slug=default --cloud_provider_slug=gcp -n; }
```
